
#include"AneuMeshLoader.h"


void AneuMeshLoader::LoadMesh(const std::string& path)
{
	std::ifstream fin;

	fin.open(path);

	if (!fin.is_open())
	{
		throw std::exception("file cannot be open ");
	}

	size_t amount;


	double x, y, z;

	size_t size;

	fin >> amount;

	fin >> size;

	node.reserve(amount);

	for (int i = 1; i <= amount; i++)
	{
		fin >> x >> y >> z;

		node.push_back(Node(x, y, z, i));
	}

	fin >> amount;

	size_t countNode;

	fin >> countNode;

	size_t N1, N2, N3, N4, idMaterial;

	finEl.reserve(amount);

	for (int i = 1; i <= amount; i++)
	{
		fin >> idMaterial >> N1 >> N2 >> N3 >> N4;

		node[N1 - 1].top = true;

		node[N2 - 1].top = true;

		node[N3 - 1].top = true;

		node[N4 - 1].top = true;

		finEl.push_back(FiniteElement(idMaterial, N1, N2, N3, N4, i));

	}

	fin >> amount;

	fin >> countNode;

	size_t idSurface;

	boundEl.reserve(amount);


	for (int i = 1; i <= amount; i++)
	{
		fin >> idSurface >> N1 >> N2 >> N3;

		node[N1 - 1].top = true;

		node[N2 - 1].top = true;

		node[N3 - 1].top = true;

		boundEl.push_back(BoundaryFiniteElement(idSurface, N1, N2, N3, i));
	}


	fin.close();


}
