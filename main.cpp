#include"AneuMeshLoader.h"
#include"MeshLoader.h"
#include <iostream>
#include<algorithm>

int main(int argc, char* argv[])
{
	MeshLoader* mesh = new AneuMeshLoader;
	std::string file_path = "MeshExample.aneu";
	try
	{
		mesh->LoadMesh(file_path);
		mesh->print();

		std::cout << "Finite Elements by 3 nodes (13,14,10):" << std::endl;
		std::vector<FiniteElement> finelems = mesh->getFiniteElementByIdNodes(13, 14, 10);
		std::for_each(finelems.begin(), finelems.end(), [](const FiniteElement& el)
			{
				std::cout << el;
			});
		finelems.clear();


		std::cout << "Finite Elements by edge(9,37):" << std::endl;
		finelems = mesh->getFiniteElementByRebro(9, 37);
		std::for_each(finelems.begin(), finelems.end(), [](const FiniteElement& el)
			{
				std::cout << el;
			});
		finelems.clear();

		std::cout << "Finite Elements by id material (1):" << std::endl;
		finelems = mesh->getFiniteElementById(1);
		std::for_each(finelems.begin(), finelems.end(), [](const FiniteElement& el)
			{
				std::cout << el;
			});
		finelems.clear();

		std::cout << "Nodes by id Surface (21):" << std::endl;
		std::vector<Node> nodes = mesh->getNodeByIdSurface(21);
		std::for_each(nodes.begin(), nodes.end(), [](const Node& el)
			{
				std::cout << el;
			});
		nodes.clear();


		std::cout << "Boundary finite elements by id Surface (21):" << std::endl;
		std::vector<BoundaryFiniteElement> boundelems = mesh->getBoundaryFiniteElementByIdSuface(21);
		std::for_each(boundelems.begin(), boundelems.end(), [](const BoundaryFiniteElement& el)
			{
				std::cout << el;
			});

		std::cout << std::setw(8) << "id:" << std::setw(12) << "Neighbours:" << std::endl;
		std::vector<std::set<size_t>> neigh_nodes = mesh->getNeighboursNodes();

		for (std::vector<std::set<size_t>>::iterator neighbours = neigh_nodes.begin(); neighbours != neigh_nodes.end(); neighbours++)
		{
			std::cout << std::setw(8) << neighbours - neigh_nodes.begin() + 1 << ":";
			std::for_each(neighbours->begin(), neighbours->end(), [](size_t _id)
				{
					std::cout << std::setw(8) << _id;
				});

			std::cout << std::endl;
		}


		mesh->insertMiddleNodes();
		std::cout << "Insert middle nodes:" << std::endl;

		std::cout << "Change mesh: " << std::endl;
		mesh->print();

	}
	catch (std::exception& ex)
	{
		std::cout << ex.what();
	}

	delete mesh;

	return 0;

}
