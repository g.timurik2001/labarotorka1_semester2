#include"MeshLoader.h"


std::vector<Node>& MeshLoader::getNode()
{
	return node;
}

std::vector< FiniteElement>& MeshLoader::getFiniteElement()
{
	return finEl;
}

std::vector< BoundaryFiniteElement>& MeshLoader::getBoundaryFiniteElement()
{
	return boundEl;
}



std::vector<Node> MeshLoader::getNodeByIdSurface(size_t _idSurface)
{
	std::vector<Node> nodeInSurface;
	std::for_each(boundEl.begin(), boundEl.end(), [&](const BoundaryFiniteElement& el)
		{
			if (el.id_surface == _idSurface)
			{
				for (auto iter_id = el.id_node.begin(); iter_id != el.id_node.end(); iter_id++)
				{
					if (std::find(nodeInSurface.begin(), nodeInSurface.end(), node[*iter_id - 1]) == nodeInSurface.end())
						nodeInSurface.push_back(node[*iter_id - 1]);
				}
			}
		});

	return nodeInSurface;
}


std::vector< FiniteElement> MeshLoader::getFiniteElementByIdNodes(size_t id1, size_t id2, size_t id3)
{
	std::vector<  FiniteElement> finiteElements;

	std::vector<  FiniteElement>::iterator current_iter = finEl.begin();

	while (current_iter != finEl.end())
	{
		current_iter = std::find_if(current_iter, finEl.end(), [id1, id2, id3](const FiniteElement& el)
			{
				return (std::find(el.id_node.begin(), el.id_node.end(), id1) != el.id_node.end()
					&& std::find(el.id_node.begin(), el.id_node.end(), id2) != el.id_node.end()
					&& std::find(el.id_node.begin(), el.id_node.end(), id3) != el.id_node.end());
			});
		if (current_iter != finEl.end())
		{
			finiteElements.push_back(*current_iter);
			++current_iter;
		}
	}

	return finiteElements;
}


std::vector < FiniteElement> MeshLoader::getFiniteElementByRebro(size_t id1, size_t id2)
{
	std::vector< FiniteElement> finiteElements;


	std::vector<FiniteElement>::iterator current_iter = finEl.begin();
	while (current_iter != finEl.end())
	{
		current_iter = std::find_if(current_iter, finEl.end(), [id1, id2](const FiniteElement& el)
			{
				return (std::find(el.id_node.begin(), el.id_node.end(), id1) != el.id_node.end() &&
					std::find(el.id_node.begin(), el.id_node.end(), id2) != el.id_node.end());
			});
		if (current_iter != finEl.end())
		{
			finiteElements.push_back(*current_iter);
			++current_iter;
		}
	}

	return finiteElements;
}



std::vector<FiniteElement> MeshLoader::getFiniteElementById(size_t _idMaterial)
{
	std::vector< FiniteElement> finEl_inMaterial;
	std::for_each(finEl.begin(), finEl.end(), [&](const FiniteElement& el)
		{
			if (el.id_material == _idMaterial)
			{
				finEl_inMaterial.push_back(el);
			}
		});
	return finEl_inMaterial;
}


std::vector <BoundaryFiniteElement>  MeshLoader::getBoundaryFiniteElementByIdSuface(size_t _idSurface)
{
	std::vector< BoundaryFiniteElement> bounEl_inSurface;
	std::for_each(boundEl.begin(), boundEl.end(), [&](const BoundaryFiniteElement& el)
		{
			if (el.id_surface == _idSurface)
			{
				bounEl_inSurface.push_back(el);
			}
		});

	return bounEl_inSurface;

}



void MeshLoader::print() const
{
	std::cout << "Nodes:" << std::endl;

	std::for_each(node.begin(), node.end(), [](const Node& el)
		{
			std::cout << (el);
		});
	std::cout << std::endl;

	std::cout << "Finite Elements:" << std::endl;
	std::for_each(finEl.begin(), finEl.end(), [](const FiniteElement& el)
		{
			std::cout << el;
		});
	std::cout << std::endl;

	std::cout << "Boundary finite elements:" << std::endl;
	std::for_each(boundEl.begin(), boundEl.end(), [](const BoundaryFiniteElement& el)
		{
			std::cout << el;
		});

}




std::vector<std::set<size_t>>  MeshLoader::getNeighboursNodes()
{
	std::vector < std::set<size_t>> result(node.size());

	for (auto finiteEl : finEl)
	{
		for (auto id_node : finiteEl.id_node)
		{
			result[id_node - 1].insert(finiteEl.id_node.begin(), finiteEl.id_node.end());
			result[id_node - 1].erase(id_node);
		}
	}

	return result;
}


void MeshLoader::insertMiddleNodes()
{
	std::unordered_set<Edge, HashEdge> container_edge;

	for (FiniteElement& finiteElem : finEl)
	{
		std::vector<size_t> current_id_node = finiteElem.id_node;

		for (auto i = current_id_node.begin(); i != current_id_node.end() - 1; i++)
		{
			for (auto j = i + 1; j != current_id_node.end(); j++)
			{
				Edge current_edge(*i, *j);

				std::unordered_set<Edge, HashEdge>::iterator search_edge = container_edge.find(current_edge);

				Node middle((node[*i].coord[0] + node[*j].coord[0]) / 2, (node[*i].coord[1] + node[*j].coord[1]) / 2, (node[*i].coord[2] + node[*j].coord[2]) / 2, node.size() + 1, false);

				if (search_edge == container_edge.end())
				{
					current_edge.middle_node = node.size() + 1;

					container_edge.insert(current_edge);

					node.push_back(middle);

					finiteElem.id_node.push_back(middle.id);

				}
				else
				{
					finiteElem.id_node.push_back(current_edge.second_node);
				}


			}

		}
	}

	for (BoundaryFiniteElement& boundary : boundEl)
	{
		std::vector<size_t> current_id_node = boundary.id_node;
		for (std::vector<size_t>::iterator i = current_id_node.begin(); i != current_id_node.end() - 1; i++)
		{
			for (std::vector<size_t>::iterator j = i + 1; j != current_id_node.end(); j++)
			{
				std::unordered_set<Edge, HashEdge>::iterator current_edge = container_edge.find(Edge(*i, *j));

				if (current_edge != container_edge.end())
				{
					boundary.id_node.push_back(current_edge->middle_node);
				}

			}
		}
	}

}
