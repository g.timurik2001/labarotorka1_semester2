#pragma once
#include"MeshLoader.h"

#include<utility>
#include<fstream> 


class AneuMeshLoader : public MeshLoader
{
public:

	void LoadMesh(const std::string& path) override;

};
