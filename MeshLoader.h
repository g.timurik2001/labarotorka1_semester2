#pragma once
#include"Struct.h"
#include<iomanip>
#include<vector>
#include<utility>
#include<algorithm>
#include<list>
#include<set>
#include<unordered_set>

class MeshLoader
{

public:


	void print() const;

	virtual void LoadMesh(const std::string& path) = 0;

	std::vector<Node>& getNode();

	std::vector<FiniteElement>& getFiniteElement();

	std::vector<BoundaryFiniteElement>& getBoundaryFiniteElement();

	std::vector<Node> getNodeByIdSurface(size_t _id);

	std::vector<FiniteElement> getFiniteElementById(size_t _id);

	std::vector <BoundaryFiniteElement> getBoundaryFiniteElementByIdSuface(size_t _id);

	std::vector<FiniteElement> getFiniteElementByIdNodes(size_t id1, size_t id2, size_t id3);

	std::vector<FiniteElement> getFiniteElementByRebro(size_t id1, size_t id2);

	std::vector<std::set<size_t>>  getNeighboursNodes();

	void insertMiddleNodes();


protected:

	std::vector<Node> node;

	std::vector<FiniteElement> finEl;

	std::vector<BoundaryFiniteElement> boundEl;

};
#pragma once
