#include<iomanip>
#include"Struct.h"

std::ostream& operator<<(std::ostream& os, const Node& node)
{
	os << std::setw(8) << node.id
		<< std::setw(12) << std::setprecision(6) << std::fixed << node.coord[0]
		<< std::setw(12) << std::setprecision(6) << std::fixed << node.coord[1]
		<< std::setw(12) << std::setprecision(6) << std::fixed << node.coord[2]
		<< std::setw(12) << std::boolalpha << node.top << std::endl;

	return os;
}

std::ostream& operator<<(std::ostream& os, const FiniteElement& fin)
{
	os << std::setw(8) << fin.id_material << std::setw(8) << fin.id_node[0] << std::setw(8) << fin.id_node[1] << std::setw(8) << fin.id_node[2] << std::setw(8) << fin.id_node[3] << std::endl;
	return os;
}

std::ostream& operator<<(std::ostream& os, const BoundaryFiniteElement& bound)
{

	os << std::setw(8) << bound.id_surface << std::setw(8) << bound.id_node[0] << std::setw(8) << bound.id_node[1] << std::setw(8) << bound.id_node[2] << std::endl;
	return os;
}

bool Node::operator==(const Node& _node) const
{
	return id == _node.id;
}

bool Node::operator!=(const Node& _node) const
{
	return id != _node.id;
}

bool FiniteElement::operator==(const FiniteElement& _finEl) const
{
	return id == _finEl.id;
}

bool FiniteElement::operator!=(const FiniteElement& _finEl) const
{
	return id != _finEl.id;
}

bool BoundaryFiniteElement::operator==(const BoundaryFiniteElement& _BoundEl) const
{
	return id == _BoundEl.id;
}

bool BoundaryFiniteElement::operator!=(const BoundaryFiniteElement& _BoundEl) const
{
	return id != _BoundEl.id;
}



Node::Node(double _x, double _y, double _z, size_t _id, bool _top)
{
	coord[0] = _x;
	coord[1] = _y;
	coord[2] = _z;
	id = _id;
	top = _top;

}

FiniteElement::FiniteElement(size_t _idMaterial, size_t N1, size_t N2, size_t N3, size_t N4, size_t _id) : id_node(4)
{

	id_material = _idMaterial;
	id_node[0] = N1;
	id_node[1] = N2;
	id_node[2] = N3;
	id_node[3] = N4;
	id = _id;
}

BoundaryFiniteElement::BoundaryFiniteElement(size_t _idSurface, size_t N1, size_t N2, size_t N3, size_t _id) : id_node(3)
{
	id_surface = _idSurface;
	id_node[0] = N1;
	id_node[1] = N2;
	id_node[2] = N3;
	id = _id;
}

Edge::Edge(size_t node1, size_t node2)
{
	first_node = node1;

	second_node = node2;
}

bool Edge::operator==(const Edge& _edge) const
{
	return (first_node == _edge.first_node && second_node == _edge.second_node)
		|| (first_node == _edge.second_node && second_node == _edge.first_node);
}

size_t HashEdge::operator()(const Edge& edge) const
{

	size_t o_seed = 0;

	if (edge.first_node > edge.second_node)
	{
		o_seed ^= std::hash<int>() (edge.first_node) + 0x9e3779b9 + (o_seed << 6) + (o_seed >> 2);
		o_seed ^= std::hash<int>() (edge.second_node) + 0x9e3779b9 + (o_seed << 6) + (o_seed >> 2);
	}
	else
	{
		o_seed ^= std::hash<int>() (edge.second_node) + 0x9e3779b9 + (o_seed << 6) + (o_seed >> 2);
		o_seed ^= std::hash<int>() (edge.first_node) + 0x9e3779b9 + (o_seed << 6) + (o_seed >> 2);
	}


	return o_seed;
}
