#pragma once
#include<iostream>
#include<array>
#include<vector>

struct Node
{
	Node(double _x, double _y, double _z, size_t _id, bool _top = false);

	Node() = default;

	~Node() = default;

	Node(const Node&) = default;

	std::array<double, 3> coord;

	size_t id;

	bool top;

	bool operator==(const Node&) const;

	bool operator!=(const Node&) const;

	friend std::ostream& operator<<(std::ostream& os, const Node& node);
};


struct FiniteElement
{
	FiniteElement(size_t _idMaterial, size_t _N1, size_t _N2, size_t _N3, size_t N4, size_t id);

	FiniteElement() = default;

	FiniteElement(const FiniteElement&) = default;

	~FiniteElement() = default;

	std::vector<size_t> id_node;

	size_t id_material;

	size_t id;

	bool operator!=(const FiniteElement&) const;

	bool operator==(const FiniteElement&) const;

	friend std::ostream& operator<<(std::ostream& os, const FiniteElement& finEl);
};


struct BoundaryFiniteElement
{
	BoundaryFiniteElement(size_t _idSurface, size_t _N1, size_t _N2, size_t _N3, size_t id);

	BoundaryFiniteElement() = default;

	BoundaryFiniteElement(const BoundaryFiniteElement&) = default;

	~BoundaryFiniteElement() = default;

	std::vector<size_t> id_node;

	size_t id;

	size_t id_surface;

	bool operator!=(const BoundaryFiniteElement&) const;

	bool operator==(const BoundaryFiniteElement&) const;

	friend std::ostream& operator<<(std::ostream& os, const BoundaryFiniteElement& boundEl);
};

struct Edge
{

	Edge() = default;

	Edge(const Edge&) = default;

	~Edge() = default;

	Edge(size_t node1, size_t node2);

	size_t first_node;

	size_t second_node;

	size_t middle_node;

	bool operator==(const Edge& _edge) const;

};

class HashEdge
{
public:
	size_t operator()(const Edge& edge) const;
};
#pragma once
